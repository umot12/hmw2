package com.example.bootjdbc.dao;

import com.example.bootjdbc.domain.Account;

import java.util.List;

public interface  CrudDao  <T>{

    void save(T entity );
    void update(T entity );
    boolean delete(T  entity );
    void deleteAll(List<T> entityList );
    void saveAll (List <T > entityList );
    List<T > findAll();
    boolean deleteById(long id);
    T  getOne(long id);


}
