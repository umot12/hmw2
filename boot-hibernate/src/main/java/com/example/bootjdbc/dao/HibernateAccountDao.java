package com.example.bootjdbc.dao;

import com.example.bootjdbc.domain.Account;


import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.util.List;



@Repository
@Slf4j
public class HibernateAccountDao implements CrudDao <Account> {
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Override
    public List<Account> findAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
          try{

        Query query = entityManager.createQuery("from Account  a");
        List<Account > resultList = query.getResultList();
        return resultList;}finally {
              entityManager.close();
          }
    }

    @Override
    public Account getOne(long id) {
        Account account = null;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            account = entityManager.find(Account.class, id);
        } catch (HibernateException ex) {
            log.error("Account not found");
        } finally {
            entityManager.close();
        }
        return account;
    }
    @Override
    public void update(Account  account ) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(account);
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Cant update");
            entityManager.getTransaction().rollback();
            throw new RuntimeException("Cant update");
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void save(Account account) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();

           entityManager.persist(account) ;
           entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Account not found");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public boolean delete(Account account) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {

            entityManager.getTransaction().begin();
            Account   account1  = entityManager.find(Account.class, account.getId());
            entityManager.remove(account1);
            entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Account not found");
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
        return true;
    }

    @Override
    public void deleteAll(List<Account> accountList) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {

            entityManager.getTransaction().begin();
          accountList.forEach(account ->entityManager.remove(account));

            entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Account not found");
            entityManager.getTransaction().rollback();

        } finally {
            entityManager.close();
        }

    }

    @Override
    public void saveAll(List<Account> accountList) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {

            entityManager.getTransaction().begin();
            accountList.forEach(account ->entityManager.persist(account));

            entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Account not found");
            entityManager.getTransaction().rollback();

        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean deleteById(long id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {

            entityManager.getTransaction().begin();

            Account account =entityManager.find(Account.class,id);
                    entityManager.remove(account) ;
            entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Account not found");
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
        return true;
    }

}