package com.example.bootjdbc.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.*;



@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Getter
@Setter
@Table(name="customers")
public class Customer  {

    @Id

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name ="customer_id")
    private  Long  id ;

    private String name;
    private String email;
    private  Integer age;

    @OneToMany (cascade = {CascadeType.MERGE,CascadeType.REMOVE },fetch =FetchType.EAGER ,mappedBy = "customer")
    private List <Account > accounts = new ArrayList<>() ;


@LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(mappedBy = "customers",cascade = {CascadeType.REMOVE})
    private List <Employer> employers = new ArrayList<>() ;

    public Customer(String name, String email, Integer age) {
        this.name = name;
        this.email = email;
        this.age = age;
    }


    @Override
    public String toString() {
        return "Customer{" +
           //     "id=" + customerId +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                '}';
    }

   // public Long  getId() {
      //  return customerId;
  //  }



    public String getName() {
        return name;
    }


    public String getEmail() {
        return email;
    }



    public Integer getAge() {
        return age;
    }



    public List<Account> getAccounts() {
        return accounts;
    }


    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }
}
