package com.example.bootjdbc.service;

import com.example.bootjdbc.domain.Account;

import java.util.List;

public interface  AccountService {
    void save(Account account );
  void update(Account account );
    boolean delete(Account account );
    void deleteAll(List<Account> accountList );
    void saveAll (List <Account > accountList );
    List<Account > findAll();
    boolean deleteById(long id);
    Account  getOne(long id);
}
