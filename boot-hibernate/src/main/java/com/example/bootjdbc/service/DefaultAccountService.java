package com.example.bootjdbc.service;


import com.example.bootjdbc.dao.HibernateAccountDao;
import com.example.bootjdbc.domain.Account;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class DefaultAccountService  implements AccountService {

    private final HibernateAccountDao accountDao;
    @Override
    public void save(Account account) {
        accountDao.save(account);
    }

    @Override
    public void update(Account account) {
        accountDao.update(account);
    }

    @Override
    public boolean delete(Account account) {
        return accountDao.delete(account) ;
    }

    @Override
    public void deleteAll(List<Account> accountList) {
        accountDao.deleteAll(accountList );
    }

    @Override
    public void saveAll(List<Account> accountList) {
        accountDao.saveAll(accountList);
    }
@Transactional(readOnly=true)
    @Override
    public List<Account> findAll() {
        return accountDao.findAll();
    }

    @Override
    public boolean deleteById(long id) {
        return accountDao.deleteById(id);
    }
    @Transactional(readOnly=true)
    @Override
    public Account getOne(long id) {
        return accountDao.getOne(id);
    }


}
