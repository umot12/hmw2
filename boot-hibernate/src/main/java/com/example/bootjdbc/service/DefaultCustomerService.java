package com.example.bootjdbc.service;




import com.example.bootjdbc.dao.HibernateCustomerDao;
import com.example.bootjdbc.domain.Customer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class DefaultCustomerService implements CustomerService  {

    private final HibernateCustomerDao customerDao;
    @Override
    public void save(Customer customer) {
        customerDao.save(customer ) ;
    }

    @Override
    public void update(Customer customer) {
     customerDao.update(customer);
    }

    @Override
    public boolean delete(Customer customer) {

     return customerDao.delete(customer);
    }

    @Override
    public void deleteAll(List<Customer> customers) {
         customerDao.deleteAll(customers ) ;
    }

    @Override
    public void saveAll(List<Customer> customers) {
           customerDao.saveAll(customers);
    }
    @Transactional(readOnly = true)
    @Override
    public List<Customer> findAll() {
         return customerDao.findAll();
    }

    @Override
    public boolean deleteById(long id) {
       return customerDao.deleteById(id) ;
    }
    @Transactional(readOnly = true)
    @Override
    public Customer getOne(Long id) {
        return customerDao.getOne(id);
    }
}
