package com.example.bootjdbc.service;


import com.example.bootjdbc.dao.EmployerDao;
import com.example.bootjdbc.domain.Account;
import com.example.bootjdbc.domain.Employer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@Transactional
@RequiredArgsConstructor
public class DefaultEmployeeService implements EmployerService {
    private final EmployerDao employeeDao;




    @Override
    @Transactional(readOnly = true, rollbackFor = NoSuchElementException.class, timeout = 1000)
    public List<Employer> getAll() {
        return employeeDao.findAll();
    }
    @Override
    public void create(Employer employee) {
        employeeDao.create(employee);
    }

    @Override
    @Transactional(readOnly = true)
    public Employer getById(Long userId) {
        return employeeDao.get(userId);
    }

    @Override
    public void update(Employer employee) {
        employeeDao.update(employee);

    }

    public void deleteById(Long id) {
        employeeDao.deleteById(id);

    }
    @Override
    public void delete(Employer employee) {
        employeeDao.delete(employee);

    }

}
